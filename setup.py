# pyktionary, simple Wiktionary scraper.
# Copyright (C) 2018 flow.gunso@gmail.com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from pyktionary import __version__
import setuptools

with open("README.md", "r") as f:
    long_description = f.read()

setuptools.setup(
    name="pyktionary",
    version=__version__,
    author="flow.gunso",
    author_email="flow.gunso@gmail.com",
    description="Simple Wiktionary scraper",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/flow.gunso/pyktionary",
    packages=setuptools.find_packages(),
    install_requires = [
        "beautifulsoup4==4.6.3",
        "requests==2.20.1"
    ],
    classifiers=[
        "Development Status :: 5 - Production/Stable",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Topic :: Internet :: WWW/HTTP :: Dynamic Content :: Wiki",
        "Topic :: Software Development :: Libraries :: Python Modules",
        "Topic :: Text Processing",
        "Topic :: Utilities",
    ],
)