## 0.7.1 | 2019/03/14
- Update pyktionary import in the README's usage and example
# 0.7 | 2019/03/14
- Add missing PyPi package dependencies
- Minor improvements to packaging:
  - Remove implicit import from pyktionary package
  - Add pyktionary version to the package and the setup 

# 0.6.1 | 2019/02/27
- Update the version number to have a badge display a succes build ...

# 0.6 | 2019/02/27
- Fix #2:
    - Fix whitespace being stripped away along with newlines (_\n_) @60173189b68f3018cdfe118154f0b33ebb4c195f
    - Workaround the excess of whitespaces @0b82b5ef577ddc8364b920263537712763869885
- Use the actual request encoding @c2a399dbfff6e375441a9a978308708e7ebffafc
- Move out of alpha into release status
- Add build badge to the README.md
 
# 0.5a0 | 2018/10/02
- initial commit
- beta release